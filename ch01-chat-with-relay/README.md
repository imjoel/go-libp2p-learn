
### 编译
> 在仓库根目录下编译

```
go build -o chat-with-relay gitee.com/joel/go-libp2p-learn/ch01-chat-with-relay
```

### 启动
> 同样是在仓库根目录

#### 1. （此窗口记做窗口A）首先按照 [ch01-relay-server/README.md](../ch01-relay-server/README.md) 启动 穿透服务 并记录下穿透服务的地址
> 假设穿透服务的输出为: `/ip4/127.0.0.1/tcp/3151/p2p/QmZAaPhi843nMSSgTLPwB9p6dyGUZzwpr4tmHRuFbAAVyZ`

#### 2. （此窗口记做窗口B）启动一个窗口，输入以下命令 并记住这个节点的ID，等待别人通过穿透连接,

> 将穿透服务地址中的 `127.0.0.1` 修改为 `服务器真实IP`; 
> 例如下面我修改为了 192.168.86.80

```
./chat-with-relay -relay /ip4/192.168.86.80/tcp/3151/p2p/QmZAaPhi843nMSSgTLPwB9p6dyGUZzwpr4tmHRuFbAAVyZ
```
> 假设窗口B输出的地址为: `/ip4/127.0.0.1/tcp/51213/p2p/QmR7FpvdksBpmse91oG4R6fGLBJRiuT4bJpmnQF9NDJWMt`

#### 3. 再启动一个窗口C, 通过穿透 A 连接窗口 B
```
./chat-with-relay \
-relay /ip4/192.168.86.80/tcp/3151/p2p/QmZAaPhi843nMSSgTLPwB9p6dyGUZzwpr4tmHRuFbAAVyZ \
-d QmR7FpvdksBpmse91oG4R6fGLBJRiuT4bJpmnQF9NDJWMt
```

#### 4. 成功

窗口C 会输出类似于以下的日志, 窗口B 也会输出 `chat connected`

```
2020/08/28 14:14:01 opening chat stream
2020/08/28 14:14:01 [INFO] chat connected!
```