
### 编译
> 在仓库根目录下编译

```
go build -o relay-server gitee.com/joel/go-libp2p-learn/ch01-relay-server
```

### 启动
> 同样是在仓库根目录
```
./relay-server -h 0.0.0.0 -l 3151
```