module gitee.com/joel/go-libp2p-learn

require (
	github.com/ipfs/go-datastore v0.4.2 // indirect
	github.com/ipfs/go-log v0.0.1
	github.com/libp2p/go-libp2p v0.5.2
	github.com/libp2p/go-libp2p-autonat-svc v0.1.0 // indirect
	github.com/libp2p/go-libp2p-circuit v0.1.4
	github.com/libp2p/go-libp2p-connmgr v0.2.1 // indirect
	github.com/libp2p/go-libp2p-core v0.3.0
	github.com/libp2p/go-libp2p-kad-dht v0.5.0 // indirect
	github.com/libp2p/go-libp2p-quic-transport v0.2.3 // indirect
	github.com/libp2p/go-libp2p-swarm v0.2.2
	github.com/libp2p/go-libp2p-tls v0.1.3 // indirect
	github.com/multiformats/go-multiaddr v0.2.0
	github.com/whyrusleeping/go-logging v0.0.1
)

go 1.13
